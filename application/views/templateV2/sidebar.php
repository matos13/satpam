   <div class="site-menubar site-menubar-light">
    <div class="site-menubar-body">
      <div>
        <div>
          <ul class="site-menu" data-plugin="menu">

            <li class="site-menu-category">Dashboard</li>
            <!-- --------------- -->
            <li class="dropdown site-menu-item has-sub">
              <a data-toggle="dropdown" href="<?php echo base_url();?>home" data-dropdown-toggle="false">
                <i class="site-menu-icon wb-grid-4" aria-hidden="true"></i>
                <span class="site-menu-title">Dashboard</span>
                 
              </a>
 
            </li>
                         <li class="site-menu-category">User</li>
            <li class="dropdown site-menu-item has-sub">
              <a data-toggle="dropdown" href="<?php echo base_url();?>masterdata/user_page" data-dropdown-toggle="false">
                <i class="site-menu-icon wb-grid-4" aria-hidden="true"></i>
                <span class="site-menu-title">User</span>
              </a>
           
            </li>
             <li class="site-menu-category">Kategori</li>
            <li class="dropdown site-menu-item has-sub">
              <a data-toggle="dropdown" href="<?php echo base_url();?>masterdata/kategori_page" data-dropdown-toggle="false">
                <i class="site-menu-icon wb-grid-4" aria-hidden="true"></i>
                <span class="site-menu-title">Kategori</span>
              </a>
           
            </li>
          <!--   <li class="site-menu-category">List Laporan</li>
            <li class="dropdown site-menu-item has-sub">
              <a data-toggle="dropdown" href="<?php echo base_url();?>laporan" data-dropdown-toggle="false">
                <i class="site-menu-icon wb-grid-4" aria-hidden="true"></i>
                <span class="site-menu-title">List Laporan</span>
              </a>
           
            </li> -->
            <li class="site-menu-category">Galery Laporan</li>
            <li class="dropdown site-menu-item has-sub">
              <a data-toggle="dropdown" href="<?php echo base_url();?>galery" data-dropdown-toggle="false">
                <i class="site-menu-icon wb-grid-4" aria-hidden="true"></i>
                <span class="site-menu-title">Galery Laporan</span>
              </a>
           
            </li>

                 <li class="dropdown site-menu-item has-sub">
                <a data-toggle="dropdown" href="javascript:void(0)" data-dropdown-toggle="false">
                        <i class="site-menu-icon wb-extension" aria-hidden="true"></i>
                        <span class="site-menu-title">Api Docs</span>
                            <span class="site-menu-arrow"></span>
                    </a>
                <div class="dropdown-menu">
                  <div class="site-menu-scroll-wrap is-list">
                    <div>
                      <div>
                        <ul class="site-menu-sub site-menu-normal-list">
                          <li class="site-menu-item">
                            <a class="animsition-link" href="<?php echo base_url();?>docs/daftar">
                              <span class="site-menu-title">Daftar</span>
                            </a>
                          </li>
                          <li class="site-menu-item">
                            <a class="animsition-link" href="<?php echo base_url();?>docs/login">
                              <span class="site-menu-title">Login</span>
                            </a>
                          </li>
                          <li class="site-menu-item">
                            <a class="animsition-link" href="<?php echo base_url();?>docs/updateprofile">
                              <span class="site-menu-title">Update Profile</span>
                            </a>
                          </li>
                          <li class="site-menu-item">
                            <a class="animsition-link" href="<?php echo base_url();?>docs/foto">
                              <span class="site-menu-title">Foto Kegiantan</span>
                            </a>
                          </li>
                          <li class="site-menu-item">
                            <a class="animsition-link" href="<?php echo base_url();?>docs/kategori">
                              <span class="site-menu-title">Kategori</span>
                            </a>
                          </li>
                              <li class="site-menu-item">
                            <a class="animsition-link" href="<?php echo base_url();?>docs/lokasi">
                              <span class="site-menu-title">Kategori</span>
                            </a>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </li>

          </ul>      
        </div>
        </div>
      </div>
    </div>
    <div class="page">