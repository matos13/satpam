<?php $this->load->view("templateV2/header.php") ?>
<?php $this->load->view("templateV2/sidebar.php") ?>
<div class="page-header">
	<h1 class="page-title">List Laporan Pemeriksaan</h1>
</div>

<div class="page-content">
	<div class="panel">
		<header class="panel-heading">
			<div class="panel-actions"></div>
			<h3 class="panel-title">List Laporan</h3>
		</header>
		 <div class="panel-body">
      <table id="tabel-laporan" class="table table-hover dataTable table-striped w-full" >
        
       <thead>
        <tr>
          <th>No</th>
          <th>Nama Satpam </th>
          <th>Kategori</th>
          <th>Lokasi</th>
          <th>Keterangan</th>
          <th>Gambar</th>
          <th>Waktu</th>
        </tr>
      </thead>
    </table>
  </div>
	</div>
</div>

<?php $this->load->view("templateV2/footer.php");
include 'laporan-js.php';
// include 'kategori-modal.php';

?>