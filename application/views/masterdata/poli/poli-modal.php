
  <div class="modal fade" id="polimodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"  aria-hidden="true">
    <div class="modal-dialog" role="document">
     <div class="modal-content">
      <div class="modal-header" style="" >
       <h5 class="modal-title" id="exampleModalLabel">INPUT DATA ESTIMASI</h5>
       <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
     <form action="#" id="form">
      <div class="form-group">
       <label class="control-label">Nama Poli</label>
       <input type="hidden" name="id_poli" id="id_poli" value="" class="form-control">
       <input type="text" id="nama" name="nama" class="form-control" placeholder="Nama Poli"> 
     </div>

     <div class="form-group">
      <label class="control-label">Status</label>
      <div class="radio-list">
        <label class="radio-inline p-0">
          <div class="radio radio-info">
            <input type="radio" name="status" id="aktif" value="1">
            <label for="radio1">Aktif</label>
          </div>
        </label>
        <label class="radio-inline">
          <div class="radio radio-info">
            <input type="radio" name="status" id="tidak_aktif" value="0">
            <label for="radio2">Tidak Aktif</label>
          </div>
        </label>
      </div>
    </div>
  </form>
  <div class="modal-footer">
   <button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
   <button  onclick="save_poli()" class="btn btn-primary">Simpan</button>
   
 </div>
</div>
</div>
</div>       