<?php $this->load->view("templateV2/header.php") ?>
<?php $this->load->view("templateV2/sidebar.php") ?>
<div class="page-header">
	<h1 class="page-title">Master Data Kategori</h1>
	<ol class="breadcrumb">
		<li class="breadcrumb-item"><a href="#">Home</a></li>
		<li class="breadcrumb-item"><a href="#">Master Data</a></li>
		<li class="breadcrumb-item active"><a href="javascript:void(0)">Master Data Katehori</a></li>
	</ol>
	<div class="page-header-actions">
		<a href="<?php echo base_url();?>masterdata/kategori_add"><button type="button" class="btn btn-sm btn-icon btn-success btn-outline btn-round"
			data-toggle="tooltip" data-original-title="Add Data">
			<i class="icon wb-plus" aria-hidden="true"></i>
		</button></a>
		<a href="<?php echo base_url();?>masterdata/kategori_page"><button type="button" class="btn btn-sm btn-icon btn-info btn-outline btn-round"
			data-toggle="tooltip" data-original-title="Refresh">
			<i class="icon wb-refresh" aria-hidden="true"></i>
		</button></a>
	</div>
</div>

<div class="page-content">
	<div class="panel">
		<header class="panel-heading">
			<div class="panel-actions"></div>
			<h3 class="panel-title">Master Data Kategori</h3>
		</header>
		 <div class="panel-body">
      <table id="tabel-kategori" class="table table-hover dataTable table-striped w-full" >
        
       <thead>
        <tr>
          <th>NO</th>
          <th>KATEGORI</th>
          <th>CREATED</th>
          <th>AKSI</th>
        </tr>
      </thead>
    </table>
  </div>
	</div>
</div>

<?php $this->load->view("templateV2/footer.php");
include 'kategori-js.php';
include 'kategori-modal.php';

?>