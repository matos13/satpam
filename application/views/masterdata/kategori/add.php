<?php $this->load->view("templateV2/header.php") ?>
<?php $this->load->view("templateV2/sidebar.php") ?>
<div id="page">

  <div class="page-header">
    <h1 class="page-title">Add Data Kategori</h1>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="#">Home</a></li>
      <li class="breadcrumb-item"><a href="#">Master Data</a></li>
      <li class="breadcrumb-item active"><a href="javascript:void(0)">Add Data Kategori</a></li>
    </ol>
    <div class="page-header-actions">
      <button type="button" class="btn btn-sm btn-icon btn-default btn-outline btn-round"
      data-toggle="tooltip" data-original-title="Edit">
      <i class="icon wb-pencil" aria-hidden="true"></i>
    </button>
    <button type="button" class="btn btn-sm btn-icon btn-default btn-outline btn-round"
    data-toggle="tooltip" data-original-title="Refresh">
    <i class="icon wb-refresh" aria-hidden="true"></i>
  </button>
  <button type="button" class="btn btn-sm btn-icon btn-default btn-outline btn-round"
  data-toggle="tooltip" data-original-title="Setting">
  <i class="icon wb-settings" aria-hidden="true"></i>
</button>
</div>
</div>

<div class="page-content">
  <div class="panel">
    <div class="panel-body container-fluid">
      <div class="row row-lg">
        <div class="col-md-6">
          <!-- Example Basic Form (Form grid) -->
          <div class="example-wrap">
            <h4 class="example-title">Form Add Kategori</h4>
            <div class="example">
              <form autocomplete="off" id="form">

                <div class="form-group">
                  <label class="form-control-label" for="inputBasicEmail">Kategori</label>
                  <input type="text" class="form-control" id="kategori" name="kategori"
                  placeholder="kategori" autocomplete="off" />
                </div>
                <div class="form-actions">
                  <a  class="btn btn-success" onclick="save_kategori()"> <i class="fa fa-check"></i> Save</a>
                  <button type="button" class="btn btn-default">Cancel</button>
                </div>
              </form>
            </div>
          </div>
          <!-- End Example Basic Form (Form grid) -->
        </div>

      </div>
    </div>
  </div>
</div>
</div>


<?php $this->load->view("templateV2/footer.php");
include 'kategori-js.php';

?>