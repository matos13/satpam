<?php $this->load->view("templateV2/header.php") ?>
<?php $this->load->view("templateV2/sidebar.php") ?>
<div class="page-header">
    <h1 class="page-title">PUT Lokasi Satpam</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <li class="breadcrumb-item"><a href="#">Api Docs</a></li>
        <li class="breadcrumb-item active"><a href="javascript:void(0)">Api Docs Lokasi Satpam</a></li>
    </ol>
</div>

<div class="page-content">
    <div class="panel">
        <header class="panel-heading">
            <div class="panel-actions"></div>
            <h3 class="panel-title">(PUT) Lokasi Satpam</h3>
        </header>
         <div class="panel-body">
 
                    <div id="store-apointment" class="panel-body" aria-expanded="true" style="">
                        <div class="form-group">
                            <label>URL</label> 
                            <input type="text" disabled="disabled" value="http://tutechdev.com/satpam/api/satpam/update_lokasi" name="" class="form-control">
                        </div>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered">
                                     <thead>
                                    <tr>
                                        <th colspan="3">Header</th>
                                    </tr> 
                                    <tr>
                                        <td>Authorization</td> 
                                        <td colspan="2"></td>
                                    </tr>
                                </thead> 
                                <thead><tr>
                                    <th colspan="3">Parameter</th>
                                </tr>
                            </thead> 
                                <thead>
                                    <tr>
                                        <th>Parameter</th> 
                                        <th>Deskripsi</th> 
                                        <th>Contoh Nilai</th>
                                    </tr>
                                </thead> 
                                <tbody>
                                    <tr>
                                        <td>latitude</td> 
                                        <td>Parameter disini adalah latitude</td> 
                                        <td>8888889</td>
                                    </tr> 
                                    <tr>
                                        <td>longitude</td>
                                        <td>Parameter disini adalah longitude user</td> 
                                        <td>6666777</td>
                                    </tr>
                                     <tr>
                                        <td>id</td>
                                        <td>Parameter disini adalah id user</td> 
                                        <td>5</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div> 
                    <strong>Response  Success:</strong> 
<pre>
{
    "status": true,
    "message": "Data lokasi berhasil diupdate.",
    "data": [
        {
            "longitude": "110.5962187",
            "latitude": "-8.0208177",
            "id": "5"
        }
    ]
}
</pre> 
        <hr>
 <strong>Response  Error:</strong> 
<pre>
{
    "status": false,
    "message": "Data lokasi gagal di update. Silakan coba kembali.",
    "data": []
}
</pre>
    </div>
<!-- </div>  -->
  </div>
    </div>
</div>


<?php $this->load->view("templateV2/footer.php");

?>