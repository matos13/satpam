<?php $this->load->view("templateV2/header.php") ?>
<?php $this->load->view("templateV2/sidebar.php") ?>
<div class="page-header">
    <h1 class="page-title">Kategori</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <li class="breadcrumb-item"><a href="#">Api Docs</a></li>
        <li class="breadcrumb-item active"><a href="javascript:void(0)">Api Docs Kategori</a></li>
    </ol>
</div>

<div class="page-content">
    <div class="panel">
        <header class="panel-heading">
            <div class="panel-actions"></div>
            <h3 class="panel-title">(GET) Kategori {all}</h3>
        </header>
         <div class="panel-body">
 
                    <div id="store-apointment" class="panel-body" aria-expanded="true" style="">
                        <div class="form-group">
                            <label>URL</label> 
                            <input type="text" disabled="disabled" value="http://tutechdev.com/satpam/api/kategori/list" name="" class="form-control">
                        </div>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered">
                                     <thead>
                                    <tr>
                                        <th colspan="3">Header</th>
                                    </tr> 
                                    <tr>
                                        <td>Authorization</td> 
                                        <td colspan="2"></td>
                                    </tr>
                                </thead> 
                                <thead><tr>
                                    <th colspan="3">Parameter</th>
                                </tr>
                            </thead> 
                                <thead>
                                    <tr>
                                        <th>Parameter</th> 
                                        <th>Deskripsi</th> 
                                        <th>Contoh Nilai</th>
                                    </tr>
                                </thead> 
                                <tbody>
                                    <tr>
                                        <td>-</td> 
                                        <td>-</td> 
                                        <td>-</td>
                                    </tr> 
                                </tbody>
                            </table>
                        </div> 
                    <strong>Response  Success:</strong> 
<pre>
{
    "status": false,
    "message": "Kategori ditemukan.",
    "data": [
        {
            "id": "1",
            "kategori": "Top Valvr 3 1/8\"",
            "created_at": "2019-08-30 21:11:31"
        },
        {
            "id": "2",
            "kategori": "Side Valve 3 1/8\" Kanan dan Kiri",
            "created_at": "2019-08-30 21:11:41"
        }
    ]
}
</pre> 
        <hr>
 <strong>Response  Error: Username/password salah</strong> 
<pre>
{
    "status": false,
    "message": "Kategori tidak ditemukan.",
    "data": []
}
</pre>
    </div>
<!-- </div>  -->
  </div>
    </div>
</div>


<?php $this->load->view("templateV2/footer.php");

?>