<?php $this->load->view("templateV2/header.php") ?>
<?php $this->load->view("templateV2/sidebar.php") ?>
<div class="page-header">
    <h1 class="page-title">POST Foto Kegiatan</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <li class="breadcrumb-item"><a href="#">Api Docs</a></li>
        <li class="breadcrumb-item active"><a href="javascript:void(0)">Api Docs Foto Kegiatan</a></li>
    </ol>
</div>

<div class="page-content">
    <div class="panel">
        <header class="panel-heading">
            <div class="panel-actions"></div>
            <h3 class="panel-title">(POST) Foto Kegiatan</h3>
        </header>
         <div class="panel-body">
 
                    <div id="store-apointment" class="panel-body" aria-expanded="true" style="">
                        <div class="form-group">
                            <label>URL</label> 
                            <input type="text" disabled="disabled" value="http://tutechdev.com/satpam/api/satpam/foto_patroli" name="" class="form-control">
                        </div>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered">
                                     <thead>
                                    <tr>
                                        <th colspan="3">Header</th>
                                    </tr> 
                                    <tr>
                                        <td>Authorization</td> 
                                        <td colspan="2"></td>
                                    </tr>
                                </thead> 
                                <thead><tr>
                                    <th colspan="3">Parameter</th>
                                </tr>
                            </thead> 
                                <thead>
                                    <tr>
                                        <th>Parameter</th> 
                                        <th>Deskripsi</th> 
                                        <th>Contoh Nilai</th>
                                    </tr>
                                </thead> 
                                <tbody>
                                    <tr>
                                        <td>id_satpam</td> 
                                        <td>Parameter disini adalah id dari satpam</td> 
                                        <td>1</td>
                                    </tr> 
                                    <tr>
                                        <td>id_kategori</td>
                                        <td>Parameter disini adalah kategori dari tempat yang difoto, ada 12 tempat</td> 
                                        <td>2</td>
                                    </tr>
                                     <tr>
                                        <td>gambar</td>
                                        <td>Parameter disini adalah foto kegiatan</td> 
                                        <td>foto.jpg</td>
                                    </tr>
                                    <tr>
                                        <td>latitude</td>
                                        <td>Parameter disini adalah latitude lokasi kegiatan</td> 
                                        <td>-7.9888889</td>
                                    </tr>
                                    <tr>
                                        <td>longitude</td>
                                        <td>Parameter disini adalah longitude lokasi kegiatan</td> 
                                        <td>87.9888889</td>
                                    </tr>
                                    <tr>
                                        <td>keterangan</td>
                                        <td>Parameter disini adalah keterangan kegiatan</td> 
                                        <td>keterangan</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div> 
                    <strong>Response  Success:</strong> 
<pre>
{
    "status": true,
    "message": "Foto berhasil di upload. Terimakasih dan selamat bekerja kembali.",
    "data": [
        {
            "id_satpam": "5",
            "id_kategori": "3",
            "latitude": "90",
            "longitude": "100",
            "gambar": "upload/foto/5/foto.jpg",
            "tgl_input": "2019-08-29 20:15:23"
            "keterangan": "keterangan"
        }
    ]
}
</pre> 
 <hr>
 <strong>Response  Error: data tidak lengkap</strong> 
<pre>
{
    "status": false,
    "message": "Data tidak lengkap, silakan lengkapi data anda.",
    "data": []
}
</pre>

    </div>

<!-- </div>  -->
  </div>
    </div>

    <!-- end panel -->
        <div class="panel">
        <header class="panel-heading">
            <div class="panel-actions"></div>
            <h3 class="panel-title">(GET) List Foto Kegiatan {all}</h3>
        </header>
         <div class="panel-body">
 
                    <div id="store-apointment" class="panel-body" aria-expanded="true" style="">
                        <div class="form-group">
                            <label>URL</label> 
                            <input type="text" disabled="disabled" value="http://tutechdev.com/satpam/api/satpam/list_foto_kegiatan?id_satpam=id_satpam" name="" class="form-control">
                        </div>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered">
                                     <thead>
                                    <tr>
                                        <th colspan="3">Header</th>
                                    </tr> 
                                    <tr>
                                        <td>Authorization</td> 
                                        <td colspan="2"></td>
                                    </tr>
                                </thead> 
                                <thead><tr>
                                    <th colspan="3">Parameter</th>
                                </tr>
                            </thead> 
                                <thead>
                                    <tr>
                                        <th>Parameter</th> 
                                        <th>Deskripsi</th> 
                                        <th>Contoh Nilai</th>
                                    </tr>
                                </thead> 
                                <tbody>
                                    <tr>
                                        <td>id_satpam</td> 
                                        <td>Parameter disini adalah id dari satpam</td> 
                                        <td>1</td>
                                    </tr> 
                                   
                                </tbody>
                            </table>
                        </div> 
                    <strong>Response  Success:</strong> 
<pre>
{
    "status": true,
    "message": "List foto kegiatan berhasil ditampilkan.",
    "data": [
        [
            {
                "id": "3",
                "id_satpam": "5",
                "id_kategori": "1",
                "alamat": null,
                "longitude": "88",
                "latitude": "77",
                "gambar": "upload/foto/5/foto-1.jpg",
                "tgl_input": "2019-08-29 20:10:51",
                "keterangan": "keterangan"
            },
            {
                "id": "4",
                "id_satpam": "5",
                "id_kategori": "2",
                "alamat": null,
                "longitude": "89",
                "latitude": "79",
                "gambar": "upload/foto/5/foto-2.jpg",
                "tgl_input": "2019-08-29 20:15:06",
                "keterangan": "keterangan"
            },
            {
                "id": "5",
                "id_satpam": "5",
                "id_kategori": "3",
                "alamat": null,
                "longitude": "100",
                "latitude": "90",
                "gambar": "upload/foto/5/foto-3.jpg",
                "tgl_input": "2019-08-29 20:15:23",
                "keterangan": "keterangan"
            }
        ]
    ]
}
</pre> 
 <hr>
 <strong>Response  Error: data tidak lengkap</strong> 
<pre>
{
    "status": false,
    "message": "List foto kegiatan gagal ditampilkan. Silakan mencoba kembali.",
    "data": []
}
</pre>

    </div>

<!-- </div>  -->
  </div>
    </div>
    <!-- end panel -->
        <div class="panel">
        <header class="panel-heading">
            <div class="panel-actions"></div>
            <h3 class="panel-title">(GET) List Foto Kegiatan {detail}</h3>
        </header>
         <div class="panel-body">
 
                    <div id="store-apointment" class="panel-body" aria-expanded="true" style="">
                        <div class="form-group">
                            <label>URL</label> 
                            <input type="text" disabled="disabled" value="http://tutechdev.com/satpam/api/satpam/list_foto_kegiatan?id_satpam=id_satpam&detail=id" name="" class="form-control">
                        </div>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered">
                                     <thead>
                                    <tr>
                                        <th colspan="3">Header</th>
                                    </tr> 
                                    <tr>
                                        <td>Authorization</td> 
                                        <td colspan="2"></td>
                                    </tr>
                                </thead> 
                                <thead><tr>
                                    <th colspan="3">Parameter</th>
                                </tr>
                            </thead> 
                                <thead>
                                    <tr>
                                        <th>Parameter</th> 
                                        <th>Deskripsi</th> 
                                        <th>Contoh Nilai</th>
                                    </tr>
                                </thead> 
                                <tbody>
                                    <tr>
                                        <td>id_satpam</td> 
                                        <td>Parameter disini adalah id dari satpam</td> 
                                        <td>1</td>
                                    </tr> 
                                    <tr>
                                        <td>detail</td> 
                                        <td>Parameter disini adalah id dari setiap foto</td> 
                                        <td>3</td>
                                    </tr> 
                                   
                                </tbody>
                            </table>
                        </div> 
                    <strong>Response  Success:</strong> 
<pre>
{
    "status": true,
    "message": "List foto kegiatan berhasil ditampilkan.",
    "data": [
        [
            {
                "id": "3",
                "id_satpam": "5",
                "id_kategori": "1",
                "alamat": null,
                "longitude": "88",
                "latitude": "77",
                "gambar": "upload/foto/5/wisdom-tooth.jpg",
                "tgl_input": "2019-08-29 20:10:51",
                "keterangan": "keterangan"
            }
        ]
    ]
}
</pre> 
 <hr>
 <strong>Response  Error: data tidak lengkap</strong> 
<pre>
{
    "status": false,
    "message": "List foto kegiatan gagal ditampilkan. Silakan mencoba kembali.",
    "data": []
}
</pre>

    </div>

<!-- </div>  -->
  </div>
    </div>
    <!-- end panel -->
</div>


<?php $this->load->view("templateV2/footer.php");

?>