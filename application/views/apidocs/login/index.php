<?php $this->load->view("templateV2/header.php") ?>
<?php $this->load->view("templateV2/sidebar.php") ?>
<div class="page-header">
    <h1 class="page-title">POST LOGIN</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <li class="breadcrumb-item"><a href="#">Api Docs</a></li>
        <li class="breadcrumb-item active"><a href="javascript:void(0)">Api Docs Login</a></li>
    </ol>
</div>

<div class="page-content">
    <div class="panel">
        <header class="panel-heading">
            <div class="panel-actions"></div>
            <h3 class="panel-title">(POST) Login</h3>
        </header>
         <div class="panel-body">
 
                    <div id="store-apointment" class="panel-body" aria-expanded="true" style="">
                        <div class="form-group">
                            <label>URL</label> 
                            <input type="text" disabled="disabled" value="http://tutechdev.com/satpam/api/login/login" name="" class="form-control">
                        </div>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered">
                                     <thead>
                                    <tr>
                                        <th colspan="3">Header</th>
                                    </tr> 
                                    <tr>
                                        <td>Authorization</td> 
                                        <td colspan="2"></td>
                                    </tr>
                                </thead> 
                                <thead><tr>
                                    <th colspan="3">Parameter</th>
                                </tr>
                            </thead> 
                                <thead>
                                    <tr>
                                        <th>Parameter</th> 
                                        <th>Deskripsi</th> 
                                        <th>Contoh Nilai</th>
                                    </tr>
                                </thead> 
                                <tbody>
                                    <tr>
                                        <td>username</td> 
                                        <td>Parameter disini adalah username</td> 
                                        <td>082147418674</td>
                                    </tr> 
                                    <tr>
                                        <td>password</td>
                                        <td>Parameter disini adalah password user (<i>Enkripsi menggunakan <b>MD5</b></i>)</td> 
                                        <td>371049</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div> 
                    <strong>Response  Success:</strong> 
<pre>
{
    "status": true,
    "message": "Login berhasil.",
    "data": [
        [
            {
                "id": "5",
                "nama": "nama",
                "username": "082147418674",
                "password": "81dc9bdb52d04dc20036dbd8313ed055",
                "no_hp": "082123321234"
            }
        ]
    ]
}
</pre> 
        <hr>
 <strong>Response  Error: Username/password salah</strong> 
<pre>
{
    "status": false,
    "message": "Login tidak berhasil.",
    "data": []
}
</pre>
    </div>
<!-- </div>  -->
  </div>
    </div>
</div>


<?php $this->load->view("templateV2/footer.php");

?>