<?php $this->load->view("templateV2/header.php") ?>
<?php $this->load->view("templateV2/sidebar.php") ?>

<?php 
$tanggal=date('m/d/Y');
if (isset($_GET['tanggal'])) {

	if ($_GET['tanggal']!='') {
		$tanggal=$_GET['tanggal'];
	}
}
?>
<div class="page">
	<div class="panel">
		

		<div class="col-md-12">
			<br>
			<form method="get" action="<?php echo base_url() ?>galery">
				
				<div class="col-md-6" style="float: right;">
					<div class="input-group">

					</div>
					<div class="input-group">
						<span class="input-group-addon">Satpam</span> 
						<select class="form-control" name="satpam">
							<option value="">Pilih Nama Satpam</option>
							<?php foreach ($satpam as $key => $value): ?>
								<option value="<?= $value['id'] ?>"><?= $value['nama'] ?></option>		
							<?php endforeach ?>
						</select>
						<span class="input-group-addon">
							<i class="icon wb-calendar" aria-hidden="true"></i>
						</span>
						<input type="text" class="form-control tanggal" name="tanggal" data-plugin="datepicker" value="<?= $tanggal?>">
						<button type="submit" id="cari" class="btn btn-warning ladda-button" data-style="zoom-in" data-plugin="ladda" data-type="progress">
							<span class="ladda-label">
								<i class="icon wb-search mr-10" aria-hidden="true"></i>Cari
							</span>
							<span class="ladda-spinner"></span>
						</button>
					</div>
				</div>
			</form>
		</div>
		<br>
		<div class="page-content">
			<?php if (!empty($laporan)) { ?>
				<ul class="blocks no-space blocks-100 blocks-xxl-3 blocks-lg-2">
					<?php 

					foreach ($laporan as $key => $value) { 	

						?>

						<li class="card card-inverse overlay overlay-hover">
							<img class="card-img overlay-scale overlay-figure" style="width: 100%;height: 500px; margin: 2px" src="upload/foto/<?= $value['id_satpam']?>/<?= $value['gambar']?>">
							<div class="card-img-overlay overlay-background overlay-background-fixed text-right vertical-align">
								<div class="vertical-align-middle">
									<div class="card-text card-divider">
										<span><?= $value['tgl_input']?></span>
									</div>
									<h5 class="card-title mb-20"><?= $value['kategori']?></h5>
									<h3 class="card-title mb-20"><?= $value['keterangan']?></h3>
									<h5 class="card-title mb-20">lokasi : (<?= $value['longitude']?>,<?= $value['latitude']?>)</h5>
									<h5 class="card-title mb-20">oleh : <?= $value['nama']?></h5>
								</div>
							</div>
						</li>
						<?php 

					}

					?>
				</ul>
				<?php 
			}else {
				?>
				<center>
					<h2>Data Tidak Ditemukan</h2>
				</center>

			<?php } ?>
		</div>
	</div>
</div>
<?php $this->load->view("templateV2/footer.php");
?>
<script type="text/javascript">
	$('#cari').click(function(){
		data=$('.tanggal').val();
		// alert(data);
		$.ajax({
			url:'<?php echo base_url() ?>galery/cari',
			type: "post",
			data: {
				"tanggal":data
			} ,
			success: function (response) {
				alert(response);
           // You will get response from your PHP page (what you echo or print)
       },

   });
	});
</script>