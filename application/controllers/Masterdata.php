<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Masterdata extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
	}

	// user controller here
	public function user_page()
	{
		$this->load->view("masterdata/user/index");
	}
	public function kategori_page()
	{
		$this->load->view("masterdata/kategori/index");
	}
	public function user_add()
	{
		$this->load->view("masterdata/user/add");
	}
	public function kategori_add()
	{
		$this->load->view("masterdata/kategori/add");
	}



}
