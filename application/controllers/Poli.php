<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Poli extends CI_Controller {
public function __construct(){
		parent::__construct();
		$this->load->model("model_poli");
		$this->load->helper('url');
	}
	public function ajax_list()
	{
		$list = $this->model_poli->get_datatables();
		$data = array();
		$no = $_POST['start'];
		$sts;
		foreach ($list as $poli) {
			$no++;
			if ($poli->status==1) {
				$sts='Aktif';
			}else{
				$sts='Tidak Aktif';
			}
			$row = array();;
			$row[] = $no;
			$row[] = $poli->nama;
			$row[] = $sts;
			$row[] = '<button data-toggle="tooltip" data-placement="top" title="Edit" type="button" class="btn btn-info btn-outline btn-circle btn-sm m-r-5" onclick="edit_poli('."'".$poli->poli_id."'".')"><i class="icon wb-edit"></i></button>
			<button data-toggle="tooltip" data-placement="top" id="sa-Delete" title="Hapus" type="button" class="btn btn-info btn-outline btn-circle btn-sm m-r-5"><i class="icon wb-trash" onclick="delete_poli('."'".$poli->poli_id."'".')"></i></button>
				  ';
			$data[] = $row;
		}
		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->model_poli->count_all(),
						"recordsFiltered" => $this->model_poli->count_filtered(),
						"data" => $data,
				);
		echo json_encode($output);
	}
	public function ajax_delete($id)
	{
		$this->model_poli->delete_by_id($id);
		echo json_encode(array("status" => TRUE));
	}
	public function ajax_edit($id)
	{
		$data=$this->model_poli->get_by_id($id);
			echo json_encode($data);
	}

	public function ajax_add()
	{
        // print_r($this->input->post());
        // exit();
		$id_poli=$this->input->post("id_poli");
		$nama=$this->input->post("nama");
		$status=$this->input->post("status");
		$data = array(
			'nama'=> $nama,
			'status'=> $status,
		);
		;
		$insert = $this->model_poli->save($data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_update()
	{
		$nama=$this->input->post("nama");
		$status=$this->input->post("status");
		$id=$this->input->post("id_poli");

		$data = array(
			'nama' => $nama, 
			'status'=> $status,
		);
		$this->model_poli->update(array('poli_id' => $id), $data);
		echo json_encode(array("status" => TRUE));
	}



}
