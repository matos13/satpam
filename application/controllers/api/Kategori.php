<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';


class Kategori extends REST_Controller {

    function __construct($config = 'rest') {
        parent::__construct($config);
    }
   
    function list_get() {
        $id = $this->get('id');
        if ($id == '') {
            $kategori = $this->db->get('kategori')->result();
        } else {
            $this->db->where('id', $id);
            $kategori = $this->db->get('kategori')->result();
        }

        if ($kategori) {
               $this->response([
                'status' => FALSE,
                'message' => 'Kategori ditemukan.',
                'data'=>$kategori
            ], REST_Controller::HTTP_OK);
            }else{
               $this->response([
                'status' => FALSE,
                'message' => 'Kategori tidak ditemukan.',
                'data'=>array()
            ], REST_Controller::HTTP_OK);
         }
    }

}