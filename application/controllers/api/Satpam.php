<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';


class Satpam extends REST_Controller {

    function __construct($config = 'rest') {
        parent::__construct($config);
    }
   
    function index_get() {
        $id = $this->get('id');
        if ($id == '') {
            $satpam = $this->db->get('satpam')->result();
        } else {
        $this->db->where('id', $id);
        $satpam = $this->db->get('satpam')->result();
        }
        if ($satpam) {
                $this->response($satpam, REST_Controller::HTTP_OK);
            }else{
                $this->response([
                    'status' => FALSE,
                    'message' => 'No users were found'
                ], REST_Controller::HTTP_NOT_FOUND); 
         }
    }

    // insert new data to satpam
    function daftar_post() {
        // print_r($this->post());
        $passone    =md5($this->post('passwordone'));
        $passtwo    =md5($this->post('passwordtwo'));
        $username   =$this->post('username');

        if ($passone != $passtwo) {
             $this->response([
                'status' => FALSE,
                'message' => 'Password tidak sama. Silakan coba kembali.',
                'data'=>array()
            ], REST_Controller::HTTP_OK);

        } else if (empty($passtwo) || empty($passone) ||  empty($username) ){
             $this->response([
                'status' => FALSE,
                'message' => 'Data tidak lengkap, silakan lengkapi data anda.',
                'data'=>array()
            ], REST_Controller::HTTP_OK);
        }

        $data = array(
                    'username'    => $username,
                    'password'    => $passtwo
                );

        $insert = $this->db->insert('satpam', $data);

        if ($insert) {
             $this->response([
                'status' => TRUE,
                'message' => 'Pendaftaran berhasil.',
                'data'=>array($data)
            ], REST_Controller::HTTP_OK);
        } else {
             $this->response([
                'status' => FALSE,
                'message' => 'Pendaftaran tidak berhasil.',
                'data'=>array()
            ], REST_Controller::HTTP_OK);
        }
    }

    // update data satpam
    function update_put() {
       // print_r($this->post());
        $username   =$this->put('username');
        $nama       =$this->put('nama');
        $no_hp      =$this->put('no_hp');
        $id         =$this->put('id');

        $data = array(
            'nama'        => $nama,
            'username'    => $username,
            'no_hp'       => $no_hp
        );

        $this->db->where('id', $id);

        $update = $this->db->update('satpam', $data);

        if ($update) {
            $this->response([
                'status' => TRUE,
                'message' => 'Data berhasil diupdate.',
                'data'=>array($data)
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => FALSE,
                'message' => 'Data gagal di update. Silakan coba kembali.',
                'data'=>array()
            ], REST_Controller::HTTP_OK);
        }
    }

        // update data satpam
    function update_lokasi_put() {
       // print_r($this->post());
        $latitude   =$this->put('latitude');
        $longitude  =$this->put('longitude');
        $id         =$this->put('id');

        $data = array(
            'longitude'   => $longitude,
            'latitude'    => $latitude,
            'id'          => $id
        );

        $this->db->where('id', $id);
        $update = $this->db->update('satpam', $data);

        if ($update) {
            $this->response([
                'status' => TRUE,
                'message' => 'Data lokasi berhasil diupdate.',
                'data'=>array($data)
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => FALSE,
                'message' => 'Data lokasi gagal di update. Silakan coba kembali.',
                'data'=>array()
            ], REST_Controller::HTTP_OK);
        }
    }

    function foto_patroli_post(){

        $id_satpam     = $this->post('id_satpam');
        $id_kategori   = $this->post('id_kategori');
        $gambar        = $this->post('gambar');
        $latitude      = $this->post('latitude');
        $longitude     = $this->post('longitude');
        $keterangan    = $this->post('keterangan');
        $created_at    = date('Y-m-d H:i:s');

        $url        = "https://maps.googleapis.com/maps/api/geocode/json";
        $token      = "?latlng="."$latitude,$longitude";
        // $token      = $token;

        $serverKey  = '&key=AIzaSyCMpVloajVzTS_WGKUnZr3KHh4XxLK28Pw';
        // $serverKey  = '';

        $headers[]  = 'Content-Type: application/json; charset=utf-8';

        $surl       = $url.$token.$serverKey;
     
        $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL             => $surl,
                CURLOPT_RETURNTRANSFER  => true,
                CURLOPT_ENCODING        => "",
                CURLOPT_MAXREDIRS       => 10,
                CURLOPT_TIMEOUT         => 30,
                CURLOPT_HTTP_VERSION    => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST   => "GET",
                CURLOPT_POSTFIELDS      => "",
                CURLOPT_HTTPHEADER      => $headers
            ));

        $response = curl_exec($curl);
        $object = json_decode($response);
        $data   = json_decode(json_encode($object->results), True);
        // print_r($data);
        // exit();
        if (empty($data)) {
            $alamat='';
        } else {
            $alamat = $data[0]['formatted_address'];
        }

        $err = curl_error($curl);
        curl_close($curl);

        $folder = "upload/foto/".$id_satpam;

        if (!file_exists($folder)) {
            mkdir("upload/foto/".$id_satpam);
        } 

        // if(!empty($_FILES['gambar']['name']))
        // {
        //     $temp = $_FILES['gambar']['tmp_name'];
        //     $filename = $_FILES['gambar']['name'];
        //     list($width_orig, $height_orig) = getimagesize($temp);
        //     $height_des = 1500;
        //     $width_des = ($height_des*$width_orig)/$height_orig;
        //     $image_p = imagecreatetruecolor($width_des, $height_des);
        //     $image = imagecreatefromjpeg($temp);

        //     imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width_des, $height_des, $width_orig, $height_orig);

        //     # code...
        //     if(file_exists("upload/foto/".$id_satpam.'/'.$filename)){
        //         unlink("upload/foto/".$id_satpam.'/'.$filename);
        //     }
        //     imagejpeg($image_p,"upload/foto/".$id_satpam.'/'.$filename);
        //     $gambar = "upload/foto/".$id_satpam.'/'.$filename;


        // }
        $b64 = $gambar;
        $bin = base64_decode($b64);
        $size = getImageSizeFromString($bin);
        if (empty($size['mime']) || strpos($size['mime'], 'image/') !== 0) {
          die('Base64 value is not a valid image');
        }
        $ext = substr($size['mime'], 6);
        if (!in_array($ext, ['png', 'gif', 'jpeg'])) {
          die('Unsupported image type');
        }
        $filename= rand(10000,100000).'.'.$ext;
        $img_file = "upload/foto/".$id_satpam.'/'.$filename;
      
        file_put_contents($img_file, $bin);

        if (empty($id_satpam) || empty($id_kategori) ||  empty($gambar) ){
            $this->response(array(
                    'status'    => FALSE,
                    'message'   => 'Transaksi gagal dilakukan. Data tidak lengkap.',
                    'data'      => array()
                ), 200);
        }

        $data = array(
            'id_satpam'     => $id_satpam,
            'id_kategori'   => $id_kategori,
            'latitude'      => $latitude,
            'longitude'     => $longitude,
            'gambar'        => $filename,
            'tgl_input'     => $created_at,
            'keterangan'    => $keterangan,
            'alamat'        => $alamat
        );

        $insert = $this->db->insert('hasil_pemeriksaan', $data);
        
        if ($insert) {

        $update = $this->db->query("UPDATE satpam set longitude='$longitude',latitude='$latitude' where id='$id_satpam' ");


        $this->response(array(
                    'status'    => TRUE,
                    'message'   => 'Foto berhasil di upload. Terimakasih dan selamat bekerja kembali.',
                    'data'      => array($data)
                ), 200);
           
        } else {
            $this->response(array(
                    'status'    => FALSE,
                    'message'   => 'Foto gagal di upload. Silakan mencoba kembali.',
                    'data'      => array()
                ), 200);
        }
    }

    
    function list_foto_kegiatan_get() {
        $id     = $this->get('id_satpam');
        $detail = $this->get('detail');

        if ($id == '') {
            $hasil_pemeriksaan = $this->db->get('hasil_pemeriksaan')->result();
        } else if ($id !='' && $detail=='') {
            $this->db->where('id_satpam', $id);
            $hasil_pemeriksaan = $this->db->get('hasil_pemeriksaan')->result();
        } else {
            $this->db->where('id', $detail);
            $this->db->where('id_satpam', $id);
            $hasil_pemeriksaan = $this->db->get('hasil_pemeriksaan')->result();
        }

        if ($hasil_pemeriksaan) {
                $this->response(array(
                    'status'    => TRUE,
                    'message'   => 'List foto kegiatan berhasil ditampilkan.',
                    'data'      => array($hasil_pemeriksaan)
                ), 200);
            }else{
                $this->response(array(
                    'status'    => FALSE,
                    'message'   => 'List foto kegiatan gagal ditampilkan. Silakan mencoba kembali.',
                    'data'      => array()
                ), 200);
         }
    }

}