<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';


class Login extends REST_Controller {

    function __construct($config = 'rest') {
        parent::__construct($config);
    }
    function login_post() {
        $password = $this->post('password');
        $username = $this->post('username');
        $password =md5($password);
        $this->db->where('password', $password)
                ->where('username',$username);

        $satpam = $this->db->get('satpam')->result();
        
        if ($satpam) {
            $this->response([
                'status' => TRUE,
                'message' => 'Login berhasil.',
                'data'=>array($satpam)
            ], REST_Controller::HTTP_OK);
        }else{
                $this->response([
                'status' => FALSE,
                'message' => 'Login tidak berhasil.',
                'data'=>array()
            ], REST_Controller::HTTP_OK);
         }
    }
}