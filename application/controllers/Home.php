<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
	}
	public function index()
	{
		$query = $this->db->query('SELECT hp.alamat,s.nama,k.kategori,hp.gambar,hp.tgl_input 
				from hasil_pemeriksaan hp 
				join satpam s on 	s.id = hp.id_satpam
				join kategori k on k.id= hp.id_kategori');
		// print_r($data);
		// exit;
		$array = json_decode(json_encode($query->result()), True);

		$query_satpam = $this->db->query('select count(id) as jumlah from satpam ');
		$satpam = json_decode(json_encode($query_satpam->result()), True);
		$query_user = $this->db->query('select count(user_id) as jumlah from user ');
		$user = json_decode(json_encode($query_user->result()), True);
		$query_laporan = $this->db->query('select count(id) as jumlah from hasil_pemeriksaan ');
		$laporan = json_decode(json_encode($query_laporan->result()), True);


		$query_satpam2 = $this->db->query('select * from satpam ');
		$data_satpam = json_decode(json_encode($query_satpam2->result()), True);

		$data['laporan']=$array;
		$data['jumlah_satpam']=$satpam;
		$data['jumlah_user']=$user;
		$data['jumlah_laporan']=$laporan;
		$data['data_satpam']=$data_satpam;
		$this->load->view("masterdata/dashboard/index",$data);
	}

	
}
