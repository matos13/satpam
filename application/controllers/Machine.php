<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Machine extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model("model_poli");
		$this->load->model("model_antrian");
		$this->load->helper('url');
	}
	public function index()
	{
		// $data['poli']=$this->model_poli->get_data_poli();
		$data['poli']=$this->model_antrian->get_antrian();
		$result = json_decode(json_encode($data['poli']),true);
		foreach ($result as $key1 => $value) {
			$hasil=$this->model_antrian->get_last_atrian($value['poli_id']);
			$nomor = json_decode(json_encode($hasil),true);
			foreach ($nomor as $key => $value) {
				$result[$key1] += [ "nomor" =>$value['nomor']];
			}
		}
		$data['poli']=$result;
		$this->load->view("counter/machine",$data);
		// $this->load->view("machine/index");
	}

	
}
