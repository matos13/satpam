<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kategori extends CI_Controller {
public function __construct(){
		parent::__construct();
		$this->load->model("model_kategori");
		$this->load->helper('url');
	}

	public function index()
	{
		$this->load->view("kategori");
	}

		public function ajax_list()
	{
		$list = $this->model_kategori->get_datatables();
		$data = array();
		$no = $_POST['start'];
		$sts;
		foreach ($list as $kategori) {
			$no++;
			$row = array();;
			$row[] = $no;
			$row[] = $kategori->kategori;
			$row[] = $kategori->created_at;
			$row[] = '<button data-toggle="tooltip" data-placement="top" title="Edit" type="button" class="btn btn-info btn-outline btn-circle btn-sm m-r-5" onclick="edit_kategori('."'".$kategori->id."'".')"><i class="icon wb-edit"></i></button>
			<button data-toggle="tooltip" data-placement="top" id="sa-Delete" title="Hapus" type="button" class="btn btn-info btn-outline btn-circle btn-sm m-r-5"><i class="icon wb-trash" onclick="delete_kategori('."'".$kategori->id."'".')"></i></button>
				  ';
			$data[] = $row;
		}
		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->model_kategori->count_all(),
						"recordsFiltered" => $this->model_kategori->count_filtered(),
						"data" => $data,
				);
		echo json_encode($output);
	}

	public function ajax_add()
	{

		$kategori	= $this->input->post("kategori");
		
		$data = array(
			'kategori' => $kategori
		);

		$insert = $this->model_kategori->save($data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_edit($id)
	{
		$data=$this->model_kategori->get_by_id($id);
			echo json_encode($data);
	}

	public function ajax_update()
	{

		$id=$this->input->post("id");
		$kategori=$this->input->post("kategori");

		$data = array(
			'kategori'=> $kategori
		);
		$update= $this->model_kategori->update(array('id' => $id), $data);

		echo json_encode(array("status" => TRUE));

	}

	public function ajax_delete($id)
	{
		$this->model_kategori->delete_by_id($id);
		echo json_encode(array("status" => TRUE));
	}


}

