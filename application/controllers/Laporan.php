<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends CI_Controller {
public function __construct(){
		parent::__construct();
		$this->load->model("model_laporan");
		$this->load->helper('url');
	}

	public function index()
	{
		$this->load->view("laporan/index");
	}

		public function ajax_list()
	{
		$list = $this->model_laporan->get_datatables();
		$data = array();
		$no = $_POST['start'];
		$sts;
		foreach ($list as $laporan) {
			// print_r($laporan);
			// exit();
			$no++;
			$row = array();;
			$row[] = $no;
			$row[] = $laporan->nama;
			$row[] = $laporan->kategori;
			$row[] = $laporan->alamat;
			$row[] = $laporan->keterangan;
			// $row[] = $laporan->gambar;
			$row[] ="<img style='display:block; width:100px;height:100px;' id='base64image'                 
       					src='upload/foto/".$laporan->id_satpam."/". ($laporan->gambar). "' />";
			$row[] = $laporan->tgl_input;
			$data[] = $row;
		}
		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->model_laporan->count_all(),
						"recordsFiltered" => $this->model_laporan->count_filtered(),
						"data" => $data,
				);
		echo json_encode($output);
	}



}

