<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Body extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model("model_poli");
		$this->load->helper('url');
	}
	public function index()
	{
		
		$this->load->view("templateV2/body");
	}

	
}
