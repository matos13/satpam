<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Galery extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model("model_laporan");
		$this->load->helper('url');
	}

	public function index()
	{	

		$tgl=date('Y-m-d');
		$where=" where DATE_FORMAT(tgl_input, '%Y-%m-%d')='$tgl'";
		if (isset($_GET['tanggal']) && isset($_GET['satpam']) ) {
			if ($_GET['tanggal']!='' && $_GET['satpam']!='' ) {
				$where=" where DATE_FORMAT(tgl_input, '%m/%d/%Y')='$_GET[tanggal]' and s.id='$_GET[satpam]'";
			}elseif ($_GET['tanggal']!='' && $_GET['satpam']=='') {
				$where=" where DATE_FORMAT(tgl_input, '%m/%d/%Y')='$_GET[tanggal]'";
			}
		}
		

		$query = $this->db->query("SELECT hp.alamat,s.nama,k.kategori,hp.gambar,hp.tgl_input,hp.keterangan,s.id as id_satpam,hp.	latitude,hp.longitude
			from hasil_pemeriksaan hp 
			join satpam s on 	s.id = hp.id_satpam
			join kategori k on k.id= hp.id_kategori".$where);
		$array = json_decode(json_encode($query->result()), True);

		$query_satpam = $this->db->query("
			select * from satpam
			");
		$satpam = json_decode(json_encode($query_satpam->result()), True);
		$data['laporan']=$array;
		// print_r($array);
		$data['satpam']=$satpam;
		$this->load->view("galery/index",$data);
	}

}

