<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Migration_create_users extends CI_Migration {
public function up() {
        $this->dbforge->add_field(array(
            'user_id' => array(
                'type' => 'INT',
                'constraint' => 100,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'user_username' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
            ),
            'user_password' => array(
                'type' => 'VARCHAR',
                'constraint' => '128',
            ),
            'user_info' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
            ),
        ));
        $this->dbforge->add_key('user_id');
        $this->dbforge->create_table('user');
    }
    public function down() {
        $this->dbforge->drop_table('user');
    }
}